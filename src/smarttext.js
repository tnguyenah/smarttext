import AutoComplete from '@tarekraafat/autocomplete.js/dist/autoComplete'
import '../resources/css/autoComplete.css'

class SmartText {
    static async #getSmartTexts () {
        const res = await fetch('/cws/v1/smarttexts/code')
        const json = await res.json()
        return json.map(({ CODE, smart_text: { FULLTEXT } }) => ({ CODE, FULLTEXT }))
    }

    static async makeSmart ({ elements }) {
        const json = await this.#getSmartTexts()
        if (elements instanceof HTMLElement) {
            elements = [elements]
        }
        elements.forEach(element => {
            const autoCompleteJS = new AutoComplete({
                pattern: /!/,
                selector: () => element,
                data: {
                    src: json,
                    keys: ['CODE'],
                    cache: true,
                },
                threshold: 1,
                resultsList: {
                    class: 'smarttext-list'
                },
                resultItem: {
                    highlight: {
                        render: true
                    },
                    class: 'smarttext-item'
                },
                query: (query) => {
                    const pattern = new RegExp(`\(^| \)${autoCompleteJS.options.pattern.source}\\w*`, 'g')
                    const querySplit = query.match(pattern)
                    if (!querySplit) {
                        return ''
                    }
                    const lastQuery = querySplit.length - 1
                    return querySplit[lastQuery].trim()
                },
                trigger (query) {
                    return query[0] === autoCompleteJS.options.pattern.source
                },
                searchEngine (query, record) {
                    const pattern = autoCompleteJS.options.pattern.source
                    if (record.indexOf(query.replace(pattern, '')) === 0) {
                        return record
                    }
                },
                events: {
                    input: {
                        selection: (event) => {
                            const feedback = event.detail
                            const input = autoCompleteJS.input
                            const selection = feedback.selection.value.FULLTEXT
                            const lastQuery = event.detail.query
                            input.value = input.value.replace(lastQuery, selection + ' ')
                        }
                    }
                }
            })
        })
    }
}
window.SmartText = SmartText
export default SmartText